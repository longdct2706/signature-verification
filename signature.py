import math
import pandas as pd
import numpy as np
from copy import deepcopy


class SigData:
    """
    Class store data read from a file
    """

    def __init__(self, filename: str, train: bool, down_sampling: int = 1, scale=100):
        self.filename = filename
        self.npoint = None
        self.genuine = None
        self.point_list = None
        self.raw_data = None
        self.pri_author = None
        self.sec_author = None
        self.train = train
        self.delta_feature = None
        self.down_sampling = down_sampling
        self.scale = scale
        # self.sig_index = None

    def __load(self):
        pass

    def __preprocess(self):
        pass

    def get_raw_data(self):
        pass

    def __len__(self):
        return self.point_list.shape[0]


class Sig2009(SigData):
    """
    Data for SigComp2009
    """

    def __init__(self, filename: str, train: bool, down_sampling: int = 1, scale=100):
        super(Sig2009, self).__init__(filename, train, down_sampling=down_sampling, scale=scale)
        self.__load()

        self.delta_feature = get_delta(self.point_list)
        self.__preprocess()
        simple_feature(self.point_list)

        self.point_list = down_sample(self.point_list, self.down_sampling)
        self.delta_feature = down_sample(self.delta_feature, self.down_sampling)

    def __load(self):
        s = self.filename.split(sep='/')
        s = s[-1].lower().replace('.hwr', '')
        if self.train:
            s = s.replace('nisdcc-', '').split(sep='_')
            self.pri_author = s[0]
            self.sec_author = s[1]
        else:
            s = s.replace('nfi-', '')
            self.pri_author = s[0:3]
            self.sec_author = s[5:8]
        self.genuine = s[0] == s[1]

        self.pri_author += '_2009'
        self.sec_author += '_2009'

        self.raw_data = pd.read_csv(self.filename, header=None, skiprows=1, sep=' ')
        self.raw_data.columns = ['x', 'y', 'p', 'yaw', 'pitch']
        # drop_index = self.raw_data[(self.raw_data['x'] <= 0) & (self.raw_data['y'] <= 0)].index
        drop_index = self.raw_data[self.raw_data['p'] == -4].index
        self.point_list = self.raw_data.drop(drop_index)

        replace_index = self.point_list['p'] == -1
        self.point_list.loc[replace_index, 'p'] = 0

        # self.point_list.drop_duplicates(subset=['x', 'y'], inplace=True)

        self.npoint = len(self.point_list)
        # self.point_list = self.point_list.loc[(self.point_list['x'] != 0) & (self.point_list['y'] != 0)]

    def __preprocess(self):
        self.point_list['normalize_x'] = rescale(self.point_list['x'], scale=self.scale)
        self.point_list['normalize_y'] = rescale(self.point_list['y'], scale=self.scale)
        self.point_list['normalize_p'] = rescale(self.point_list['p'], scale=self.scale)
        self.point_list['normalize_yaw'] = rescale(self.point_list['yaw'], scale=self.scale)
        self.point_list['normalize_pitch'] = rescale(self.point_list['pitch'], scale=self.scale)

    def get_raw_data(self):
        return self.raw_data.reindex(['x', 'y', 'p', 'yaw', 'pitch'], axis='columns')


class Sig2011(SigData):
    """
    Data for SigComp2009
    """

    def __init__(self, filename: str, train: bool, down_sampling: int = 1, scale=100):
        super(Sig2011, self).__init__(filename, train, down_sampling=down_sampling, scale=scale)
        self.__load()

        self.delta_feature = get_delta(self.point_list)
        self.__preprocess()
        simple_feature(self.point_list)

        self.point_list = down_sample(self.point_list, self.down_sampling)
        self.delta_feature = down_sample(self.delta_feature, self.down_sampling)

    def __load(self):
        s = self.filename.split(sep='/')
        s = s[-1].lower().replace('.hwr', '')
        s = s.split(sep='_')
        if len(s) == 3:
            self.genuine = False
            if self.train:
                self.pri_author = s[0]
                self.sec_author = s[1]
            else:
                self.pri_author = s[1]
                self.sec_author = s[2]
        elif len(s) == 2:
            self.genuine = True
            if self.train:
                self.pri_author = self.sec_author = s[0]
            else:
                self.pri_author = self.sec_author = s[1]
        else:
            raise Exception('Invalid filename')

        self.pri_author += '_2011'
        self.sec_author += '_2011'

        self.raw_data = pd.read_csv(self.filename, header=None, sep=' ')
        self.raw_data.columns = ['x', 'y', 'p']
        # self.raw_data = super().down_sample()
        # drop_index = self.raw_data[(self.raw_data['x'] <= 0) & (self.raw_data['y'] <= 0)].index
        drop_index = self.raw_data[self.raw_data['p'] == -4].index
        self.point_list = self.raw_data.drop(drop_index)

        replace_index = self.point_list['p'] == -1
        self.point_list.loc[replace_index, 'p'] = 0

        # self.point_list.drop_duplicates(subset=['x', 'y'], inplace=True)

        self.npoint = len(self.point_list)
        # self.point_list = self.point_list.loc[(self.point_list['x'] != 0) & (self.point_list['y'] != 0)]

    def __preprocess(self):
        self.point_list['normalize_x'] = rescale(self.point_list['x'], scale=self.scale)
        self.point_list['normalize_y'] = rescale(self.point_list['y'], scale=self.scale)
        self.point_list['normalize_p'] = rescale(self.point_list['p'], scale=self.scale)

    def get_raw_data(self):
        return self.point_list.reindex(['x', 'y', 'p'], axis='columns')


def get_delta(df: pd.DataFrame, eps=1e-6):
    # df = pd.DataFrame(columns=['delta_x', 'delta_y', 'delta_p'])
    # for i in range(len(a) - 1):
    #     df.loc[i, :] = a.loc[i + 1, :] - a.loc[i, :]
    df1 = df.reindex(index=df[1:].index).reset_index().drop('index', axis=1)
    df2 = df.reindex(index=df[:-1].index).reset_index().drop('index', axis=1)

    if len(df.columns) == 5:
        df1 = df1.drop(['yaw', 'pitch'], axis=1)
        df2 = df2.drop(['yaw', 'pitch'], axis=1)

    df = df1 - df2
    df.columns = ['delta_x', 'delta_y', 'delta_p']

    replace_index = (df['delta_x'] == 0).index
    df.loc[replace_index, 'delta_x'] = eps

    replace_index = (df['delta_y'] == 0).index
    df.loc[replace_index, 'delta_y'] = eps

    # df['delta_p'] = rescale(df['delta_p'])

    df['d'] = np.square(df['delta_x']) + np.square(df['delta_y'])
    df['sin'] = df['delta_y'] / np.sqrt(df['d'])
    df['cos'] = df['delta_x'] / np.sqrt(df['d'])
    return df


def augmentation(sig: SigData, k=2):
    step = len(sig.point_list) // k
    sig_list = []
    for _ in range(k):
        sig_list.append(deepcopy(sig))

    for i in range(k):
        sig_list[i].point_list = sig.point_list.iloc[step * i:step * (i + 1), :]

    return sig_list


def down_sample(df: pd.DataFrame, k: int):
    return df[::k]


def rescale(a: np.ndarray, scale=100, eps=1e-6) -> np.ndarray:
    b = np.copy(a).astype(dtype=float)
    max_element = b.max()
    min_element = b.min()
    dividend = max(max_element - min_element, eps)
    return (b - min_element) / dividend * scale


def simple_feature(df: pd.DataFrame):
    x = df['normalize_x'].values
    y = df['normalize_y'].values

    def velocity(var: np.ndarray, eps=1e-6) -> np.ndarray:
        v = np.zeros(len(var), dtype='float32')
        v[0] = v[-1] = eps  # very small to guarantee no divide by zero
        for i in range(1, len(v) - 1):
            v[i] = max(var[i - 1] + var[i + 1], eps)
            v[i] /= 2.0
        return v

    df['v_x'] = velocity(x)
    df['v_y'] = velocity(y)
    df['v'] = np.sqrt(np.square(df['v_x'].values) + np.square(df['v_y'].values))

    # acceleration is velocity of velocity
    df['a_x'] = velocity(df['v_x'].values)
    df['a_y'] = velocity(df['v_y'].values)

    # Centripetal acceleration
    df['a_c'] = (df['v_x'] * df['a_y'] + df['v_y'] * df['a_x']) / df['v']

    # Cosine of the angle between signature curve and x-axis
    def cos_curve_x(var_x: np.ndarray, var_y: np.ndarray, eps=1e-6) -> np.ndarray:
        cos = np.zeros(len(var_x), dtype='float32')
        cos[-1] = eps
        for i in range(1, len(cos) - 1):
            s1 = max(var_x[i + 1] - var_x[i], eps)
            s2 = max(var_y[i + 1] - var_y[i], eps)
            cos[i] = s1 / math.sqrt(s1 ** 2 + s2 ** 2)
        return cos

    df['cos_xy'] = cos_curve_x(x, y)

    # Cosine of the angle between absolute velocity and x-axis
    df['cos_v'] = df['v_x'] / df['v']
