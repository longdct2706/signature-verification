import visdom
import numpy as np
import torch
from sklearn.manifold import TSNE
from metrics import count_correct_with_classes


class Plotter:
    def __init__(self, author_list, env_name='main'):
        self.viz = visdom.Visdom()
        self.env = env_name
        self.plot = {}
        self.author_list = author_list.tolist()

    def tsne_plot(self, x, y):
        X_embedded = TSNE(n_iter=2000).fit_transform(x)
        y += 1
        if 'tsne' not in self.plot:
            self.plot['tsne'] = self.viz.scatter(X=torch.from_numpy(X_embedded),
                                                 Y=torch.from_numpy(y),
                                                 env=self.env,
                                                 opts=dict(
                                                     # legend=self.author_list,
                                                     title='TSNE plot',
                                                 ))
        else:
            self.viz.scatter(X=torch.from_numpy(X_embedded),
                             Y=torch.from_numpy(y),
                             env=self.env,
                             win=self.plot['tsne'],
                             update='replace')

    def accuracy_bar_plot(self, x, y, title):
        df = count_correct_with_classes(x, y, self.author_list)
        acc = np.array(df['correct'].values / df['total'].values)
        self.plot['bar'] = self.viz.bar(X=torch.from_numpy(acc), env=self.env, opts=dict(
            rownames=self.author_list,
            title=title
        ))
