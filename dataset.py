import random
import numpy as np
from sklearn.preprocessing import LabelEncoder

import torch
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pad_sequence, pack_padded_sequence

from feature_extraction import extract_feature, extract_delta_feature


class PairSignature(Dataset):
    def __init__(self,
                 dataset,
                 use_delta: bool = False):
        """
        Create train dataset
        :param dataset: Input dataset, list of pair of signatures
        """
        self.dataset = dataset
        self.use_delta = use_delta

    def __getitem__(self, index):
        sig1, sig2 = self.dataset[index]
        if sig1.genuine and sig2.genuine and sig1.sec_author == sig2.sec_author:
            label = 1.0
        else:
            label = 0.0

        if self.use_delta:
            sig1 = extract_delta_feature(sig1)
            sig2 = extract_delta_feature(sig2)
        else:
            sig1 = extract_feature(sig1)
            sig2 = extract_feature(sig2)

        y = torch.from_numpy(np.array([label], dtype=np.float32))
        return sig1, sig2, y

    def __len__(self):
        return len(self.dataset)


class SingleSignature(Dataset):
    def __init__(self,
                 dataset,
                 label_encoder=None,
                 use_delta: bool = False):
        """
        Create dataset for single signature training
        :param dataset: signature data
        :param label_encoder: label encoder
        :param use_delta: boolean, check use delta feature or not
        """
        self.dataset = dataset
        self.use_delta = use_delta
        authors = [sig.sec_author if sig.genuine else 'forged' for sig in self.dataset]
        # authors = [sig.sec_author for sig in self.dataset if sig.genuine] + \
        #     [sig.sec_author + '_forged' for sig in self.dataset if not sig.genuine]
        if label_encoder is None:
            self.label_encoder = LabelEncoder()
            self.label_encoder.fit(list(set(authors)))
        else:
            self.label_encoder = label_encoder
        self.y = self.label_encoder.transform(authors)

    def __getitem__(self, index):
        sig = self.dataset[index]

        if self.use_delta:
            X = extract_delta_feature(sig)
        else:
            X = extract_feature(sig)

        y = torch.from_numpy(np.array([self.y[index]], dtype=np.float32))

        return X, y

    def __len__(self):
        return len(self.dataset)


class ProcessedPairSignature(Dataset):
    def __init__(self, dataloader, model, use_gpu=True):
        self.X1, self.X2, self.y = siamese_feature_extract(dataloader, model, use_gpu=use_gpu)

    def __getitem__(self, index):
        x1 = self.X1[index]
        x2 = self.X2[index]
        y = torch.from_numpy(np.array([self.y[index]], dtype=np.float32))

        return x1, x2, y

    def __len__(self):
        return len(self.X1)


def get_autoencoder_loader(data,
                           batch_size,
                           use_delta: bool = True,
                           valid_ratio=0.2,
                           seed=6,
                           num_workers=4,
                           pin_memory=False):
    authors = [sig.sec_author if sig.genuine else 'forged' for sig in data]
    label_encoder = LabelEncoder()
    label_encoder.fit(list(set(authors)))

    # train, valid, _, _ = train_test_split(data, authors, test_size=valid_ratio, random_state=seed)
    train, valid = train_test_split(data, valid_ratio, random_state=seed)
    train_dataset = SingleSignature(train, use_delta=use_delta, label_encoder=label_encoder)
    valid_dataset = SingleSignature(valid, use_delta=use_delta, label_encoder=label_encoder)

    train_loader = DataLoader(
        train_dataset, batch_size=batch_size,
        num_workers=num_workers, pin_memory=pin_memory,
        collate_fn=pad_collate_single
    )

    valid_loader = DataLoader(
        valid_dataset, batch_size=batch_size,
        num_workers=num_workers, pin_memory=pin_memory,
        collate_fn=pad_collate_single
    )
    return train_loader, valid_loader


def get_train_valid_loader(train_data,
                           valid_data,
                           batch_size,
                           model,
                           use_delta: bool = True,
                           shuffle: bool = False,
                           use_gpu: bool = True,
                           num_workers=4,
                           pin_memory=False):
    """
    Utility function for loading and returning train and valid multi-process
    iterators over the dataset.
    If using CUDA, num_workers should be set to `1` and pin_memory to `True`.
    :param train_data: training data, read from file
    :param valid_data: validation data, read from file
    :param batch_size: size of a mini-batch
    :param model: autoencoder model
    :param use_delta: whether use delta feature
    :param shuffle: whether use shuffle on training set
    :param use_gpu: whether use gpu
    :param num_workers: number of sub-processes to use when loading the dataset. Set to `1` if using GPU.
    :param pin_memory: whether to copy tensors into CUDA pinned memory. Set it to `True` if using GPU.
    :return:
    """
    train_dataset = PairSignature(train_data, use_delta=use_delta)
    train_loader = DataLoader(
        train_dataset, batch_size=batch_size, shuffle=shuffle,
        num_workers=num_workers, pin_memory=pin_memory,
        collate_fn=pad_collate_pair
    )

    valid_dataset = PairSignature(valid_data, use_delta=use_delta)
    valid_loader = DataLoader(
        valid_dataset, batch_size=batch_size, shuffle=False,
        num_workers=num_workers, pin_memory=pin_memory,
        collate_fn=pad_collate_pair
    )

    processed_train = ProcessedPairSignature(train_loader, model, use_gpu)
    train_loader = DataLoader(
        processed_train, batch_size=batch_size, shuffle=shuffle,
        num_workers=num_workers, pin_memory=pin_memory,
    )

    processed_valid = ProcessedPairSignature(valid_loader, model, use_gpu)
    valid_loader = DataLoader(
        processed_valid, batch_size=batch_size, shuffle=shuffle,
        num_workers=num_workers, pin_memory=pin_memory,
    )

    return train_loader, valid_loader


def get_test_loader(test_data,
                    model,
                    use_delta: bool = False,
                    use_gpu: bool = True,
                    batch_size=1,
                    num_workers=4,
                    pin_memory=False):
    """
    Utility function for loading and returning test multi-process
    iterators over the dataset.
    If using CUDA, num_workers should be set to `1` and pin_memory to `True`.
    :param test_data: test data, read from file
    :param model: autoencoder model
    :param use_delta: whether use delta feature
    :param use_gpu: whether use gpu
    :param batch_size: size of a mini-batch
    :param num_workers: number of sub-processes to use when loading the dataset. Set to `1` if using GPU.
    :param pin_memory: whether to copy tensors into CUDA pinned memory. Set it to `True` if using GPU.
    :return:
    """
    test_dataset = PairSignature(test_data, use_delta=use_delta)
    test_loader = DataLoader(
        test_dataset, batch_size=batch_size, shuffle=False,
        num_workers=num_workers, pin_memory=pin_memory,
        collate_fn=pad_collate_pair
    )
    processed_test = ProcessedPairSignature(test_loader, model, use_gpu)
    test_loader = DataLoader(
        processed_test, batch_size=batch_size, shuffle=False,
        num_workers=num_workers, pin_memory=pin_memory,
    )

    return test_loader


def pad_collate_pair(batch):
    x1 = [torch.from_numpy(s[0]).type('torch.FloatTensor') for s in batch]
    x2 = [torch.from_numpy(s[1]).type('torch.FloatTensor') for s in batch]
    y = torch.stack([s[2] for s in batch])

    len_x1 = [len(s[0]) for s in batch]
    len_x2 = [len(s[1]) for s in batch]

    pad_seq_x1 = pad_sequence(x1, batch_first=True)
    pad_seq_x2 = pad_sequence(x2, batch_first=True)

    packed_x1 = pack_padded_sequence(pad_seq_x1, len_x1, batch_first=True, enforce_sorted=False)
    packed_x2 = pack_padded_sequence(pad_seq_x2, len_x2, batch_first=True, enforce_sorted=False)
    return packed_x1, packed_x2, y


def pad_collate_single(batch):
    X = [torch.from_numpy(s[0]).type('torch.FloatTensor') for s in batch]
    y = torch.stack([s[1] for s in batch])

    len_X = [len(s[0]) for s in batch]

    pad_seq_X = pad_sequence(X, batch_first=True)

    packed_X = pack_padded_sequence(pad_seq_X, len_X, batch_first=True, enforce_sorted=False)
    return packed_X, y


def train_test_split(data, test_size, random_state=420):
    train = []
    test = []
    rng = random.Random(random_state)
    authors = set([sig.sec_author if sig.genuine else 'forged' for sig in data])
    le = LabelEncoder()
    le.fit(list(authors))

    sig_each_author = []
    for _ in range(len(authors)):
        sig_each_author.append([])
    for sig in data:
        if not sig.genuine:
            idx = le.transform(['forged'])
        else:
            idx = le.transform([sig.sec_author])
        sig_each_author[int(idx)].append(sig)

    for sigs in sig_each_author:
        rng.shuffle(sigs)
        split_idx = int((1 - test_size) * len(sigs))
        train += sigs[:split_idx]
        test += sigs[split_idx:]

    rng.shuffle(train)
    rng.shuffle(test)
    return train, test


def siamese_feature_extract(dataloader, model, use_gpu=True):
    X1 = []
    X2 = []
    y_true = []
    if use_gpu:
        model.cuda()
    for x1, x2, y in dataloader:
        if use_gpu:
            x1, x2 = x1.cuda(), x2.cuda()

        _, x1 = model(x1)
        _, x2 = model(x2)
        x1 = x1.cpu().detach().numpy()
        x2 = x2.cpu().detach().numpy()

        x1 = np.split(x1, x1.shape[0], axis=0)
        x2 = np.split(x2, x2.shape[0], axis=0)

        X1 += x1
        X2 += x2
        y_true.append(y.squeeze(1).numpy())

    X1 = np.concatenate(X1, axis=0)
    X2 = np.concatenate(X2, axis=0)
    y_true = np.concatenate(y_true, axis=0)
    return X1, X2, y_true
