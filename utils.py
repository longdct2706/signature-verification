import os
import json
import shutil
import numpy as np


class AverageMeter(object):
    """
    Computes and stores the average and
    current value.
    """

    def __init__(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def rolling_window(a, window):
    shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
    strides = a.strides + (a.strides[-1],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)


def prepare_dirs(config, autoencoder):
    num_model = get_num_model(config)
    for path in [config.ckpt_dir, config.logs_dir, config.plot_dir]:
        path = os.path.join(path, num_model)
        if not os.path.exists(path):
            os.makedirs(path)
        if autoencoder:
            path = os.path.join(path, 'autoencoder')
        else:
            path = os.path.join(path, 'siamese')
        if not os.path.exists(path):
            os.makedirs(path)
        if config.flush:
            shutil.rmtree(path)
            if not os.path.exists(path):
                os.makedirs(path)


def save_config(config, autoencoder: bool):
    num_model = get_num_model(config)
    model_dir = os.path.join(config.ckpt_dir, num_model)
    if autoencoder:
        model_dir = os.path.join(model_dir, 'autoencoder')
        filename = 'params_autoencoder.json'
    else:
        model_dir = os.path.join(model_dir, 'siamese')
        filename = 'params_siamese.json'
    param_path = os.path.join(model_dir, filename)

    if not os.path.isfile(param_path):
        print("[*] Model Checkpoint Dir: {}".format(model_dir))

        all_params = config.__dict__
        with open(param_path, 'w') as fp:
            json.dump(all_params, fp, indent=4, sort_keys=True)
    else:
        raise ValueError


def load_config(config):
    num_model = get_num_model(config)
    model_dir = os.path.join(config.ckpt_dir, num_model)
    filename = 'params.json'
    param_path = os.path.join(model_dir, filename)
    params = json.load(open(param_path))
    print("[*] Loaded layer hyperparameters.")
    wanted_keys = [
        'layer_end_momentums', 'layer_init_lrs', 'layer_l2_regs'
    ]
    hyperparams = dict((k, params[k]) for k in wanted_keys if k in params)
    return hyperparams


def get_num_model(config):
    num_model = config.num_model
    error_msg = "[!] model number must be >= 1."
    assert num_model > 0, error_msg
    return 'exp_' + str(num_model)
