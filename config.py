import argparse

arg_lists = []
parser = argparse.ArgumentParser(description='Signature Verification')


def str2bool(v):
    return v.lower() in ('true', '1')


def add_argument_group(name):
    arg = parser.add_argument_group(name)
    arg_lists.append(arg)
    return arg


# data params
data_arg = add_argument_group('Data Params')
data_arg.add_argument('--batch_size', type=int, default=64,
                      help='# of images in each batch of data')
data_arg.add_argument('--shuffle', type=str2bool, default=True,
                      help='Whether to shuffle the dataset between epochs')
# model params
model_arg = add_argument_group('Model Params')
model_arg.add_argument('--lstm_mode', type=str, default='last',
                       choices=['last', 'sum', 'avg', 'max'],
                       help='LSTM output extract method')
model_arg.add_argument('--input_dim', type=int, default=11,
                       help='Dimension of input layer')
model_arg.add_argument('--hidden_dim', type=int, default=32,
                       help='Dimension of hidden layer')
model_arg.add_argument('--num_layer', type=int, default=1,
                       help='Number of hidden layer')
model_arg.add_argument('--feature_dim', type=int, default=8,
                       help='Dimension of extracted feature')

# training params
train_arg = add_argument_group('Training Params')
train_arg.add_argument('--is_train', type=str2bool, default=True,
                       help='Whether to train or test the model')
train_arg.add_argument('--use_delta', type=str2bool, default=False,
                       help='Whether use delta feature')
train_arg.add_argument('--train_patience', type=int, default=10,
                       help='Number of epochs to wait before stopping train')
train_arg.add_argument('--epochs', type=int, default=100,
                       help='# of epochs to train for')
train_arg.add_argument('--dropout', type=float, default=0.0,
                       help='Dropout rate')
train_arg.add_argument('--lr', type=float, default=1e-3,
                       help='Learning rate. Default = 1e-3')
train_arg.add_argument('--weight_decay', type=float, default=1e-4,
                       help='Weight decay rate. Default = 1e-4')
train_arg.add_argument('--margin', type=float, default=0.2,
                       help='Margin for contrastive loss. Default = 0.2')
train_arg.add_argument('--gamma_c', type=float, default=0.5,
                       help='Center loss weight')
train_arg.add_argument('--lr_c', type=float, default=0.5,
                       help='Learning rate for center loss')
train_arg.add_argument('--autoencoder', type=str2bool, required=True,
                       help='Choose to train autoencoder or siamese network')

# other params
misc_arg = add_argument_group('Misc.')
misc_arg.add_argument('--flush', type=str2bool, default=False,
                      help='Whether to delete ckpt + log files for model no.')
misc_arg.add_argument('--num_model', type=int, default=1, required=True,
                      help='Model number used for unique checkpoint')
misc_arg.add_argument('--use_gpu', type=str2bool, default=True,
                      help="Whether to run on the GPU")
misc_arg.add_argument('--device', type=int, default=0,
                      choices=[0, 1, 2],
                      help="Select which gpu to run on")
misc_arg.add_argument('--best', type=str2bool, default=True,
                      help='Load best model or most recent for testing')
misc_arg.add_argument('--random_seed', type=int, default=420,
                      help='Seed to ensure reproducibility')
misc_arg.add_argument('--plot_dir', type=str, default='./plots/',
                      help='Directory in which plots are stored')
misc_arg.add_argument('--ckpt_dir', type=str, default='./models/',
                      help='Directory in which to save model checkpoints')
misc_arg.add_argument('--logs_dir', type=str, default='./logs/',
                      help='Directory in which logs wil be stored')
misc_arg.add_argument('--resume', type=str2bool, default=False,
                      help='Whether to resume training from checkpoint')


def get_config():
    config, unparsed = parser.parse_known_args()

    if config.use_delta:
        config.input_dim = 4
    else:
        config.input_dim = 11

    if not config.is_train:
        config.flush = False

    return config, unparsed
