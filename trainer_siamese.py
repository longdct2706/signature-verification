import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.utils
import numpy as np

import os
import time
import shutil
from tqdm import tqdm

from utils import AverageMeter, get_num_model
from metrics import eer_metrics, count_correct
from loss_function import ContrastiveLoss, PositiveContrastiveLoss, NegativeContrastiveLoss, CenterLoss


class TrainerSiamese(object):
    def __init__(self, config, data_loader, siamese_model, autoencoder_model):
        self.config = config

        if config.is_train:
            self.train_loader = data_loader[0]
            self.valid_loader = data_loader[1]
            self.num_train = len(self.train_loader.dataset)
            self.num_valid = len(self.valid_loader.dataset)
            self.train_patience = self.config.train_patience
        else:
            self.test_loader = data_loader
            self.num_test = len(self.test_loader.dataset)

        if config.is_train:
            num_positive_train, num_negative_train = investigate_dataloader(self.train_loader)
            num_positive_valid, num_negative_valid = investigate_dataloader(self.valid_loader)
            self.data_info = {
                'train_positive': num_positive_train,
                'train_negative': num_negative_train,
                'valid_positive': num_positive_valid,
                'valid_negative': num_negative_valid
            }

            print("[*] Train on {} sample pairs, validate on {} trials".format(
                self.num_train, self.num_valid)
            )
            print(
                '[*] Train: {} genuine signature pairs, {} forged signature pairs'.format(
                    self.data_info['train_positive'], self.data_info['train_negative']
                )
            )
            print(
                '[*] Validation: {} genuine signature pairs, {} forged signature pairs'.format(
                    self.data_info['valid_positive'], self.data_info['valid_negative']
                )
            )
        else:
            num_positive_test, num_negative_test = investigate_dataloader(self.test_loader)
            self.data_info = {
                'test_positive': num_positive_test,
                'test_negative': num_negative_test
            }

            print(
                '[*] Test: {} genuine signature pairs, {} forged signature pairs'.format(
                    self.data_info['test_positive'], self.data_info['test_negative']
                )
            )

        # path params
        self.num_model = get_num_model(config)

        self.ckpt_dir = os.path.join(config.ckpt_dir, self.num_model)
        self.logs_dir = os.path.join(config.logs_dir, self.num_model)
        self.plot_dir = os.path.join(config.plot_dir, self.num_model)

        self.ckpt_dir = os.path.join(self.ckpt_dir, 'siamese')
        self.logs_dir = os.path.join(self.logs_dir, 'siamese')
        self.plot_dir = os.path.join(self.plot_dir, 'siamese')

        self.siamese_model = siamese_model
        self.autoencoder_model = autoencoder_model
        model_path = os.path.join(self.ckpt_dir, '../autoencoder.tar')
        self.autoencoder_model.load_state_dict(torch.load(model_path))

        if config.use_gpu:
            self.siamese_model.cuda()
            self.autoencoder_model.cuda()

        # model params
        self.num_params = sum(
            [p.data.nelement() for p in self.siamese_model.parameters()]
        )

        print('[*] Number of model parameters: {:,}'.format(self.num_params))

        # misc params
        self.resume = config.resume
        self.use_gpu = config.use_gpu
        self.dtype = (
            torch.cuda.FloatTensor if self.use_gpu else torch.FloatTensor
        )

        # optimization params
        self.best = config.best
        self.best_valid_acc = 0.
        self.best_valid_eer = 1.0
        self.threshold = 0.0
        self.epochs = config.epochs
        self.start_epoch = 0
        self.counter = 0

        self.lr = config.lr
        self.weight_decay = config.weight_decay

        self.optimizer = optim.Adam(
            self.siamese_model.parameters(), lr=self.lr, weight_decay=self.weight_decay,
        )

        # self.loss = ContrastiveLoss(config.margin)
        # self.p_loss = PositiveContrastiveLoss()
        # self.n_loss = NegativeContrastiveLoss(config.margin)
        self.loss = nn.BCEWithLogitsLoss()

    def train(self):
        if self.resume:
            self.load_checkpoint(best=False)

        # create train and validation log files
        train_file = open(os.path.join(self.logs_dir, 'train.csv'), 'w')
        valid_file = open(os.path.join(self.logs_dir, 'valid.csv'), 'w')

        train_losses = []
        for epoch in range(self.start_epoch, self.epochs):

            print('Epoch: {}/{}'.format(epoch + 1, self.epochs))

            train_loss = self.train_one_epoch(epoch, train_file)
            train_losses.append(train_loss)
            valid_eer, valid_acc, valid_thresh = self.test(epoch=epoch, file=valid_file, is_validate=True)

            # check for improvement
            is_best = valid_eer < self.best_valid_eer
            msg = "Train loss: {:.3f} - val acc: {:.3f} - val eer: {:.3f}"
            if is_best:
                msg += " [*]"
                self.counter = 0
                self.threshold = valid_thresh
            print(msg.format(train_loss, valid_acc, valid_eer))

            # checkpoint the model
            if not is_best:
                self.counter += 1
            if self.counter > self.train_patience:
                print("[!] No improvement in a while, stopping training.")
                return
            self.best_valid_eer = min(valid_eer, self.best_valid_eer)
            self.save_checkpoint(
                {
                    'epoch': epoch + 1,
                    'model_state': self.siamese_model.state_dict(),
                    'optim_state': self.optimizer.state_dict(),
                    'best_valid_eer': self.best_valid_eer,
                    'threshold': self.threshold
                }, is_best
            )
        # release resources
        train_file.close()
        valid_file.close()

    def train_one_epoch(self, epoch, file):
        train_batch_time = AverageMeter()
        train_losses = AverageMeter()

        # switch to train mode
        self.siamese_model.train()

        tic = time.time()
        p_loss = AverageMeter()
        n_loss = AverageMeter()
        with tqdm(total=self.num_train) as pbar:
            for i, (x1, x2, y) in enumerate(self.train_loader):
                if self.use_gpu:
                    x1, x2 = x1.cuda(), x2.cuda()

                # batch_size = x1.shape[0]
                batch_size = len(y)

                out = self.siamese_model(x1, x2).cpu()

                # y_true = y.squeeze(1).cpu()
                loss = self.loss(out, y)
                # p_loss.update(self.p_loss(out, y).item(), batch_size)
                # n_loss.update(self.n_loss(out, y).item(), batch_size)

                # compute gradients and update
                self.optimizer.zero_grad()
                loss.backward()
                # torch.nn.utils.clip_grad_value_(self.siamese_model.parameters(), 1.0)
                self.optimizer.step()

                # store batch statistics
                toc = time.time()
                train_losses.update(loss.item(), batch_size)
                train_batch_time.update(toc - tic)
                tic = time.time()

                pbar.set_description(
                    (
                        # "Total-Positive-Negative loss: {:.3f} {:.3f} {:.3f}".format(
                        #     train_losses.val, p_loss.val, n_loss.val
                        "Loss: {:.3f}".format(
                            train_losses.val
                        )
                    )
                )
                pbar.update(batch_size)

                # log loss
                iteration = (epoch * len(self.train_loader)) + i
                file.write('{},{}\n'.format(
                    iteration, train_losses.val)
                )

            return train_losses.avg

    def test(self, epoch=None, file=None, is_validate: bool = False):
        # Load checkpoint if test phase
        if not is_validate:
            self.load_checkpoint(best=self.best)

        # switch to evaluate mode
        self.siamese_model.eval()

        batch_time = AverageMeter()
        tic = time.time()

        y_true = []
        y_score = []
        if is_validate:
            total = self.num_valid
            dataloader = self.valid_loader
        else:
            total = self.num_test
            dataloader = self.test_loader
        with tqdm(total=total) as pbar:
            for i, (x1, x2, y) in enumerate(dataloader):
                if self.use_gpu:
                    x1, x2 = x1.cuda(), x2.cuda()

                batch_size = len(y)

                out = self.siamese_model(x1, x2).cpu()

                out = torch.sigmoid(out)
                y_true.append(y.numpy())
                y_score.append(out.detach().numpy())

                # store batch statistics
                toc = time.time()
                batch_time.update(toc - tic)
                tic = time.time()

                if is_validate:
                    pbar.set_description('Validation')
                else:
                    pbar.set_description('Test')

                pbar.update(batch_size)

        y_true = np.concatenate(y_true).ravel()
        y_score = np.concatenate(y_score).ravel()
        # compute acc and log
        accuracy, positive_correct, negative_correct, eer, thresh = eer_metrics(y_true, y_score)

        if is_validate:
            file.write('{},{},{},{}\n'.format(
                epoch, accuracy, eer, thresh, positive_correct, negative_correct)
            )
            print(
                "[*] Valid Acc: {}/{} ({:.3f}%)".format(
                    positive_correct + negative_correct, self.num_valid, accuracy
                )
            )
            print(
                "[*] Positive correct: {}/{}\n"
                "[*] Negative correct: {}/{}".format(positive_correct, self.data_info['valid_positive'],
                                                     negative_correct, self.data_info['valid_negative'])
            )
            print(
                '[*] Threshold: {}'.format(thresh)
            )
        else:
            print(
                "[*] Test Acc: {}/{} ({:.3f}%)".format(
                    positive_correct + negative_correct, self.num_test, accuracy
                )
            )
            print(
                "[*] Positive correct: {}/{}\n"
                "[*] Negative correct: {}/{}".format(positive_correct, self.data_info['test_positive'],
                                                     negative_correct, self.data_info['test_negative'])
            )

        return eer, accuracy, thresh

    def save_checkpoint(self, state, is_best):
        filename = 'model_ckpt.tar'
        ckpt_path = os.path.join(self.ckpt_dir, filename)
        torch.save(state, ckpt_path)

        if is_best:
            filename = 'best_model_ckpt.tar'
            shutil.copyfile(
                ckpt_path, os.path.join(self.ckpt_dir, filename)
            )

    def load_checkpoint(self, best=False):
        print("[*] Loading model from {}".format(self.ckpt_dir))

        filename = 'model_ckpt.tar'
        if best:
            filename = 'best_model_ckpt.tar'
        ckpt_path = os.path.join(self.ckpt_dir, filename)
        ckpt = torch.load(ckpt_path)

        # load variables from checkpoint
        self.start_epoch = ckpt['epoch']
        self.best_valid_eer = ckpt['best_valid_eer']
        self.siamese_model.load_state_dict(ckpt['model_state'])
        self.optimizer.load_state_dict(ckpt['optim_state'])
        self.threshold = ckpt['threshold']

        if best:
            print(
                "[*] Loaded {} checkpoint @ epoch {} "
                "with best valid eer of {:.3f}".format(
                    filename, ckpt['epoch'], ckpt['best_valid_eer'])
            )
        else:
            print(
                "[*] Loaded {} checkpoint @ epoch {}".format(
                    filename, ckpt['epoch'])
            )


def investigate_dataloader(dataloader):
    y_true = []
    for _, (_, _, y_batch) in enumerate(dataloader):
        for y in y_batch:
            y_true.append(y.item())

    num_positive = 0
    num_negative = 0
    for y in y_true:
        num_positive += y == 0
        num_negative += y == 1

    return num_positive, num_negative
