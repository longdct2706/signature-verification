import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.utils

import os
import time
import shutil
from tqdm import tqdm
import numpy as np

from utils import AverageMeter, get_num_model
from metrics import count_correct
from loss_function import CenterLoss


class TrainerAutoencoder(object):
    def __init__(self, config, data_loader, model, plotter=None):
        self.config = config

        self.train_loader = data_loader[0]
        self.valid_loader = data_loader[1]
        self.num_train = len(self.train_loader.dataset)
        self.num_valid = len(self.valid_loader.dataset)
        self.train_patience = self.config.train_patience

        self.plotter = plotter
        self.model = model
        if config.use_gpu:
            self.model.cuda()

        # self.params = list(self.model.parameters())
        self.loss = nn.CrossEntropyLoss()
        self.softmax = nn.LogSoftmax(dim=-1)
        # self.loss = nn.NLLLoss()
        self.center_loss = CenterLoss(num_classes=self.model.output_dim,
                                      feat_dim=self.model.hidden_dim * 2,
                                      use_gpu=False)
        self.gamma_c = config.gamma_c
        self.lr_c = config.lr_c
        # self.params += list(self.center_loss.parameters())

        # model params
        self.num_params = sum(
            [p.data.nelement() for p in self.model.parameters()]
        )
        self.num_model = get_num_model(config)

        print('[*] Number of model parameters: {:,}'.format(self.num_params))

        # path params
        self.ckpt_dir = os.path.join(config.ckpt_dir, self.num_model)
        self.logs_dir = os.path.join(config.logs_dir, self.num_model)
        self.plot_dir = os.path.join(config.plot_dir, self.num_model)

        self.ckpt_dir = os.path.join(self.ckpt_dir, 'autoencoder')
        self.logs_dir = os.path.join(self.logs_dir, 'autoencoder')
        self.plot_dir = os.path.join(self.plot_dir, 'autoencoder')

        # misc params
        self.resume = config.resume
        self.use_gpu = config.use_gpu
        self.dtype = (
            torch.cuda.FloatTensor if self.use_gpu else torch.FloatTensor
        )

        # optimization params
        self.best = config.best
        self.best_valid_acc = 0.
        self.epochs = config.epochs
        self.start_epoch = 0
        self.counter = 0

        self.lr = config.lr
        self.weight_decay = config.weight_decay

        self.optimizer = optim.Adam(
            self.model.parameters(), lr=self.lr, weight_decay=self.weight_decay,
        )
        self.optimizer_center_loss = optim.SGD(
            self.center_loss.parameters(), lr=self.lr_c
        )

    def train(self):
        if self.resume:
            self.load_checkpoint(best=False)

        # create train and validation log files
        train_file = open(os.path.join(self.logs_dir, 'train.csv'), 'w')
        valid_file = open(os.path.join(self.logs_dir, 'valid.csv'), 'w')

        train_losses = []
        for epoch in range(self.start_epoch, self.epochs):

            print('Epoch: {}/{}'.format(epoch + 1, self.epochs))

            train_loss, correct = self.train_one_epoch(epoch, train_file)
            train_losses.append(train_loss)
            valid_acc = self.validate(epoch=epoch, file=valid_file)

            # check for improvement
            is_best = valid_acc > self.best_valid_acc
            msg = "Train loss: {:.3f} - train acc: {:.3f} - val acc: {:.3f}"
            if is_best:
                msg += " [*]"
                self.counter = 0
            print(msg.format(train_loss, correct / self.num_train, valid_acc))

            # checkpoint the model
            if not is_best:
                self.counter += 1
            if self.counter > self.train_patience:
                print("[!] No improvement in a while, stopping training.")
                return
            self.best_valid_acc = max(valid_acc, self.best_valid_acc)
            self.save_checkpoint(
                {
                    'epoch': epoch + 1,
                    'model_state': self.model.state_dict(),
                    'optim_state': self.optimizer.state_dict(),
                    'best_valid_acc': self.best_valid_acc,
                }, is_best
            )
        # release resources
        train_file.close()
        valid_file.close()

    def train_one_epoch(self, epoch, file):
        train_batch_time = AverageMeter()
        train_losses = AverageMeter()

        # switch to train mode
        self.model.train()

        tic = time.time()
        correct = 0
        all_out = []
        all_feat_out = []
        all_y_true = []
        with tqdm(total=self.num_train) as pbar:
            for i, (x, y) in enumerate(self.train_loader):
                if self.use_gpu:
                    x = x.cuda()

                batch_size = len(y)

                out, feat_out = self.model(x)
                out, feat_out = out.cpu(), feat_out.cpu()

                y_true = y.long().squeeze(1)
                loss = self.loss(out, y_true) + self.gamma_c * self.center_loss(feat_out, y_true)
                # loss = self.loss(out, y_true)

                # compute gradients and update
                self.optimizer.zero_grad()
                self.optimizer_center_loss.zero_grad()
                loss.backward()
                self.optimizer.step()
                # torch.nn.utils.clip_grad_value_(self.params, 1.0)
                for param in self.center_loss.parameters():
                    param.grad.data *= (1. / self.gamma_c)
                self.optimizer_center_loss.step()

                # store batch statistics
                toc = time.time()
                train_losses.update(loss.item(), batch_size)
                train_batch_time.update(toc - tic)
                tic = time.time()

                correct_iter = count_correct(self.softmax(out), y_true)
                correct += correct_iter

                all_out.append(out.detach().numpy())
                all_feat_out.append(feat_out.detach().numpy())
                all_y_true.append(y.long().squeeze())

                pbar.set_description(
                    (
                        "Loss {:.4f} Acc {:.4f}".format(
                            train_losses.val, correct_iter / batch_size
                        )
                    )
                )
                pbar.update(batch_size)

                # log loss
                iter = (epoch * len(self.train_loader)) + i
                file.write('{},{}\n'.format(
                    iter, train_losses.val)
                )

        if self.plotter is not None:
            all_out = np.concatenate(all_out, axis=0)
            all_y_true = np.concatenate(all_y_true, axis=0)
            self.plotter.accuracy_bar_plot(all_out, all_y_true, 'Accuracy train epoch {}'.format(epoch + 1))

        print(
            "[*] Train Acc: {}/{} {:.3f}%".format(
                correct, self.num_train, correct / self.num_train
            )
        )
        return train_losses.avg, correct

    def validate(self, epoch=None, file=None):
        # switch to evaluate mode
        self.model.eval()

        batch_time = AverageMeter()
        tic = time.time()

        total = self.num_valid
        dataloader = self.valid_loader
        correct = 0
        all_out = []
        all_feat_out = []
        all_y_true = []
        with tqdm(total=total) as pbar:
            for i, (x, y) in enumerate(dataloader):
                if self.use_gpu:
                    x = x.cuda()

                batch_size = len(y)

                out, feat_out = self.model(x)
                out = out.cpu()
                feat_out = feat_out.cpu()

                y_true = y.long().squeeze(1)
                correct += count_correct(self.softmax(out), y_true)

                all_out.append(out.detach().numpy())
                all_feat_out.append(feat_out.detach().numpy())
                all_y_true.append(y.long().squeeze())

                # store batch statistics
                toc = time.time()
                batch_time.update(toc - tic)
                tic = time.time()

                pbar.set_description('Validation')
                pbar.update(batch_size)

        if self.plotter is not None:
            all_out = np.concatenate(all_out, axis=0)
            all_y_true = np.concatenate(all_y_true, axis=0)
            self.plotter.accuracy_bar_plot(all_out, all_y_true, 'Accuracy valid epoch {}'.format(epoch + 1))

        accuracy = correct / self.num_valid
        file.write('{},{}\n'.format(
            epoch, accuracy)
        )
        print(
            "[*] Valid Acc: {}/{} {:.3f}%".format(
                correct, self.num_valid, accuracy
            )
        )
        return accuracy

    def save_checkpoint(self, state, is_best):
        filename = 'model_ckpt.tar'
        ckpt_path = os.path.join(self.ckpt_dir, filename)
        torch.save(state, ckpt_path)

        if is_best:
            filename = 'best_model_ckpt.tar'
            shutil.copyfile(
                ckpt_path, os.path.join(self.ckpt_dir, filename)
            )

    def load_checkpoint(self, best=False):
        print("[*] Loading model from {}".format(self.ckpt_dir))

        filename = 'model_ckpt.tar'
        if best:
            filename = 'best_model_ckpt.tar'
        ckpt_path = os.path.join(self.ckpt_dir, filename)
        ckpt = torch.load(ckpt_path)

        # load variables from checkpoint
        self.start_epoch = ckpt['epoch']
        self.best_valid_acc = ckpt['best_valid_acc']
        self.model.load_state_dict(ckpt['model_state'])
        self.optimizer.load_state_dict(ckpt['optim_state'])

        if best:
            print(
                "[*] Loaded {} checkpoint @ epoch {} "
                "with best valid acc of {:.3f}".format(
                    filename, ckpt['epoch'], ckpt['best_valid_acc'])
            )
        else:
            print(
                "[*] Loaded {} checkpoint @ epoch {}".format(
                    filename, ckpt['epoch'])
            )

    def save_model(self, best=False):
        if best:
            src = 'best_model_ckpt.tar'
        else:
            src = 'model_ckpt.tar'
        dest = 'autoencoder.tar'

        src_path = os.path.join(self.ckpt_dir, src)
        dest_path = os.path.join(self.ckpt_dir, '../{}'.format(dest))

        state = torch.load(src_path)
        torch.save(state['model_state'], dest_path)
