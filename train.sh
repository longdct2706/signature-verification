#!/usr/bin/env bash
#alias python=python3
python3 main.py --num_model "$1" \
                --device "$2" \
                --batch_size 16 \
                --epochs 200 \
                --train_patience 20 \
                --lr 0.01 \
                --weight_decay 0.001 \
                --dropout 0.5 \
                --gamma_c 0.5 \
                --lr_c 0.5 \
                --margin 1.0 \
                --hidden_dim 64 \
                --num_layer 3 \
                --output_dim 64 \
                --use_delta True \
                --lstm_mode max \
                --flush True \
                --best False \
                --autoencoder "$3" \
                --use_gpu False

