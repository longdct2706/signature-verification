#!/usr/bin/env bash
python3 main.py --is_train False \
                --num_model "$1" \
                --device "$2" \
                --input_dim 11 \
                --hidden_dim 128 \
                --num_layer 1 \
                --output_dim 64 \
                --use_delta False \
                --lstm_mode max \
                --autoencoder False


