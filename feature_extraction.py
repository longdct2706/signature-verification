from signature import SigData
import numpy as np


def extract_feature(sig: SigData) -> np.ndarray:
    """
    Extract data from a SignatureData
    :param sig: SignatureData target
    :return: X, y: data and label
    """
    columns = ['normalize_x', 'normalize_y', 'normalize_p',
               'v_x', 'v_y', 'v',
               'a_x', 'a_y', 'a_c',
               'cos_xy', 'cos_v']
    feature = sig.point_list.reindex(columns, axis='columns').values

    return feature


def extract_delta_feature(sig: SigData) -> np.ndarray:
    """
    Extract data from a SignatureData
    :param sig: SignatureData target
    :return: X, y: data and label
    """
    columns = ['sin', 'cos', 'delta_p', 'd']
    feature = sig.delta_feature.reindex(columns, axis='columns').values

    return feature
