import os

from file_reader import DataContainer
from dataset import *
from trainer_siamese import TrainerSiamese
from trainer_autoencoder import TrainerAutoencoder
from config import get_config
from utils import prepare_dirs, save_config, get_num_model
from rnn.pair_training import Siamese
from rnn.single_training import SingleRNN
from plot import Plotter

import torch

torch.multiprocessing.set_sharing_strategy('file_system')

# os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
# os.environ['CUDA_VISIBLE_DEVICES'] = '2'

DATA_PATH = {
    'train_combination': 'data/train_combination.pkl',
    'valid_combination': 'data/valid_combination.pkl',
    'test_combination': 'data/test_combination.pkl',
    'train_data': 'data/train_data.pkl',
    'valid_data': 'data/valid_data.pkl',
    'test_data': 'data/test_data.pkl'
}


def siamese(configuration):
    siamese_model = Siamese(feature_dim=configuration.hidden_dim * 2, dropout=configuration.dropout)
    autoencoder_model = SingleRNN(input_dim=configuration.input_dim,
                                  hidden_dim=configuration.hidden_dim,
                                  num_layer=configuration.num_layer,
                                  output_dim=configuration.output_dim,  # hardcoded - useless now anw
                                  feature_dim=configuration.feature_dim,
                                  dropout=configuration.dropout,
                                  use_gpu=configuration.use_gpu,
                                  mode=configuration.lstm_mode)

    num_model = get_num_model(configuration)
    ckpt_dir = os.path.join(configuration.ckpt_dir, num_model)
    ckpt_dir = os.path.join(ckpt_dir, 'siamese')
    model_path = os.path.join(ckpt_dir, '../autoencoder.tar')
    autoencoder_model.load_state_dict(torch.load(model_path))

    print('[*] Finish loading model')

    kwargs = {}
    if configuration.use_gpu:
        torch.cuda.manual_seed(configuration.random_seed)
        kwargs = {'num_workers': 1, 'pin_memory': True}
    if configuration.is_train:
        train_data = DataContainer.load(DATA_PATH['train_combination']).trials
        valid_data = DataContainer.load(DATA_PATH['valid_combination']).trials
        data_loader = get_train_valid_loader(
            train_data, valid_data,
            configuration.batch_size,
            autoencoder_model,
            use_delta=configuration.use_delta,
            use_gpu=configuration.use_gpu,
            shuffle=configuration.shuffle,
            **kwargs
        )
    else:
        test_data = DataContainer.load(DATA_PATH['test_combination']).trials
        data_loader = get_test_loader(
            test_data,
            autoencoder_model,
            use_delta=configuration.use_delta,
            use_gpu=configuration.use_gpu,
            batch_size=configuration.batch_size,
            **kwargs
        )

    print('[*] Finish loading data')

    if configuration.is_train and not configuration.resume:
        try:
            save_config(configuration, configuration.autoencoder)
        except ValueError:
            print(
                "[!] Samples already exist. Either change the model number,",
                "or delete the json file and rerun.",
                sep=' '
            )
            raise SystemExit
    else:
        pass

    trainer = TrainerSiamese(configuration, data_loader, siamese_model, autoencoder_model)

    if configuration.is_train:
        trainer.train()
    else:
        trainer.test()


def autoencoder(configuration):
    kwargs = {}
    if configuration.use_gpu:
        torch.cuda.manual_seed(configuration.random_seed)
        kwargs = {'num_workers': 1, 'pin_memory': True}
    data = DataContainer.load(DATA_PATH['train_data']).trials

    data_loader = get_autoencoder_loader(
        data,
        configuration.batch_size,
        use_delta=configuration.use_delta,
        seed=config.random_seed,
        valid_ratio=0.15,
        **kwargs
    )

    print('[*] Finish loading data')

    if configuration.is_train and not configuration.resume:
        try:
            save_config(configuration, configuration.autoencoder)
        except ValueError:
            print(
                "[!] Samples already exist. Either change the model number,",
                "or delete the json file and rerun.",
                sep=' '
            )
            raise SystemExit
    else:
        pass

    output_dim = len(data_loader[0].dataset.label_encoder.classes_)
    model = SingleRNN(input_dim=configuration.input_dim,
                      hidden_dim=configuration.hidden_dim,
                      num_layer=configuration.num_layer,
                      output_dim=output_dim,
                      feature_dim=configuration.feature_dim,
                      dropout=configuration.dropout,
                      use_gpu=configuration.use_gpu,
                      mode=configuration.lstm_mode)

    # Plot using visdom
    plotter = Plotter(data_loader[0].dataset.label_encoder.classes_)

    trainer = TrainerAutoencoder(configuration, data_loader, model, plotter)

    trainer.train()
    trainer.save_model(config.best)


if __name__ == '__main__':
    # Load config
    config, unparsed = get_config()
    if config.use_gpu:
        os.environ['CUDA_VISIBLE_DEVICES'] = str(config.device)

    if config.use_delta:
        config.input_dim = 4
    else:
        config.input_dim = 11

    if not config.autoencoder:
        config.flush = False
        config.output_dim = 23

    prepare_dirs(config, config.autoencoder)

    torch.manual_seed(config.random_seed)

    if config.autoencoder:
        autoencoder(config)
    else:
        siamese(config)
