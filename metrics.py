from sklearn.metrics import roc_curve
from scipy.optimize import brentq
from scipy.interpolate import interp1d
import torch
import pandas as pd
import numpy as np


def eer_metrics(y_true, y_score):
    # fpr, tpr, thresholds = roc_curve(y_true, y_score, pos_label=1)
    fpr, tpr, thresholds = roc_curve(y_true, y_score)

    eer = brentq(lambda x: 1. - x - interp1d(fpr, tpr)(x), 0., 1.)
    thresh = interp1d(fpr, thresholds)(eer)

    accuracy, positive_correct, negative_correct = accuracy_with_threshold(y_true, y_score, thresh)

    return accuracy, positive_correct, negative_correct, eer, thresh


def accuracy_with_threshold(y_true, y_score, thresh):
    positive_correct = 0
    negative_correct = 0
    for i in range(len(y_true)):
        if y_true[i] == 0:  # 2 genuine sigs
            positive_correct += y_score[i] <= thresh  # true positive
        else:  # other cases
            negative_correct += y_score[i] > thresh  # true negative

    accuracy = (positive_correct + negative_correct) / len(y_true)

    return accuracy, positive_correct, negative_correct


def count_correct(y_score, y_true):
    # y = y_score.exp()
    y_pred = torch.argmax(y_score, dim=1)
    correct = torch.sum(torch.eq(y_pred, y_true)).item()

    return correct


def count_correct_with_classes(y_score, y_true, author_list):
    y_pred = np.argmax(y_score, axis=1)
    total = np.zeros(len(author_list))
    correct = np.zeros(len(author_list))
    for i in range(len(y_true)):
        author = int(y_true[i])
        total[author] += 1
        if y_true[i] == y_pred[i]:
            correct[author] += 1
    # for i in range(len(author_list)):
    #     total = 0
    #     correct = 0
    #     for j in range(len(y_true)):
    #         if y_true[j].item() == i:
    #             total += 1
    #             if y_pred[j].item() == y_true[j].item():
    #                 correct += 0
    #     df.append({'author': author_list[i],
    #                'total': total,
    #                'correct': correct}, ignore_index=True)
    df = pd.DataFrame({'author': author_list, 'total': total, 'correct': correct})

    return df
