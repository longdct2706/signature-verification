import os
import time
from tqdm import tqdm
from typing import List, Dict
import gzip
import pickle

from signature import SigData, Sig2009, Sig2011, augmentation


def walkdir(folder):
    """Walk through each files in a directory"""
    for dirpath, dirs, files in os.walk(folder):
        for filename in files:
            yield os.path.abspath(os.path.join(dirpath, filename))


class FileReader:
    def __init__(self,
                 path: Dict[str, str],
                 scale: float = 1,
                 down_sampling: int = 1,
                 split: int = 2,
                 seed: int = 6):
        self.path = path
        self.scale = scale
        self.down_sampling = down_sampling
        self.split = split
        self.seed = seed

    def read_train_data(self):
        """
        Import training data files
        :return: List of SignatureData (train)
        """
        train_2009 = self.__read_train_2009()

        train_2011_real, train_2011_fake = self.__read_train_2011()
        return train_2009 + train_2011_real + train_2011_fake
        # return train_2011_real + train_2011_fake

    def __read_train_2009(self) -> List[SigData]:
        """
        Import training data SigComp2009
        :return: List of SignatureData (train)
        """
        training_path = self.path['train_2009']
        # Preprocess the total files count
        file_counter = 0
        for _ in walkdir(training_path):
            file_counter += 1

        train_set = []
        for filepath in tqdm(walkdir(training_path), total=file_counter, unit="files"):
            sig = Sig2009(filepath, train=True, scale=self.scale, down_sampling=self.down_sampling)
            # train_set.append(sig)
            if sig.genuine:
                train_set += augmentation(sig, k=self.split)
            else:
                train_set.append(sig)

        # Since there are huge imbalance in 2009 dataset,
        # I will randomly drop 1/2 of the forged signatures
        # rng = random.Random(seed)
        # forged_sig = [sig for sig in train_set if not sig.genuine]
        # to_be_removed = rng.sample(forged_sig, k=(len(forged_sig) // 2))

        # train = [sig for sig in train_set if sig not in to_be_removed]
        # print('Length of 2009 training set: {}'.format(len(train)))

        time.sleep(0.2)
        return train_set

    def __read_train_2011(self):
        """
        Import training data SigComp2011
        :return: List of SignatureData (train)
        """
        real_path = self.path['train_2011_real']
        fake_path = self.path['train_2011_fake']

        # Preprocess the total files count
        file_counter = 0
        for _ in walkdir(real_path):
            file_counter += 1

        train_2011_real = []
        for filepath in tqdm(walkdir(real_path), total=file_counter, unit="files"):
            sig = Sig2011(filepath, train=True, scale=self.scale, down_sampling=self.down_sampling)
            train_2011_real.append(sig)

        time.sleep(0.2)

        # Preprocess the total files count
        file_counter = 0
        for _ in walkdir(fake_path):
            file_counter += 1

        train_2011_fake = []
        for filepath in tqdm(walkdir(fake_path), total=file_counter, unit="files"):
            sig = Sig2011(filepath, train=True, scale=self.scale, down_sampling=self.down_sampling)
            train_2011_fake.append(sig)

        time.sleep(0.2)
        return train_2011_real, train_2011_fake

    def read_test_data(self):
        """
        Return test dataset (SigComp2011)
        :return: List of Signature data (test) - ref set and questioned set
        """
        test_path_ref = self.path['test_reference']
        test_path_question = self.path['test_question']
        print('Reference set')
        file_counter = 0
        for _ in walkdir(test_path_ref):
            file_counter += 1

        test_set_ref = []
        for filepath in tqdm(walkdir(test_path_ref), total=file_counter, unit='files'):
            sig = Sig2011(filepath, train=False, scale=self.scale, down_sampling=self.down_sampling)
            test_set_ref.append(sig)

        print('Question set')
        file_counter = 0
        for _ in walkdir(test_path_question):
            file_counter += 1

        test_set_question = []
        for filepath in tqdm(walkdir(test_path_question), total=file_counter, unit='files'):
            sig = Sig2011(filepath, train=False, scale=self.scale, down_sampling=self.down_sampling)
            test_set_question.append(sig)

        time.sleep(0.2)
        return test_set_ref, test_set_question

    def read_valid_data(self):
        """
        Return validation dataset
        :return: List of Signature data (validation) - ref set and questioned set
        """
        valid_ref = self.path['valid_reference']
        valid_question = self.path['valid_question']
        print('PROCESS VALIDATION SET')
        file_counter = 0
        for _ in walkdir(valid_ref):
            file_counter += 1

        ref_set = []
        for filepath in tqdm(walkdir(valid_ref), total=file_counter, unit='files'):
            sig = Sig2009(filepath, train=False, scale=self.scale, down_sampling=self.down_sampling)
            ref_set.append(sig)
            # ref_set += augmentation(sig)

        file_counter = 0
        for _ in walkdir(valid_question):
            file_counter += 1

        question_set = []
        for filepath in tqdm(walkdir(valid_question), total=file_counter, unit='files'):
            sig = Sig2009(filepath, train=False, scale=self.scale, down_sampling=self.down_sampling)
            question_set.append(sig)
            # question_set += augmentation(sig)

        # rng = random.Random(seed)
        valid_set_ref = ref_set
        valid_set_question = question_set

        # question_author = set([sig.sec_author for sig in question_set])
        #
        # drop half of negative example
        # for author in question_author:
        #     sig_list = [sig for sig in question_set if sig.sec_author == author and not sig.genuine]
        #     choose_k_sig = len(sig_list) // 1
        #     valid_set_question += rng.sample(sig_list, k=choose_k_sig)
        #
        # valid_set_question += [sig for sig in question_set if sig.genuine]
        #
        # print(
        #     'Length validation reference set: {}\n'
        #     'Length validation questioned set: {}'.format(len(valid_set_ref), len(valid_set_question))
        # )

        return valid_set_ref, valid_set_question


def get_max_len(dataset: List[SigData]) -> int:
    """
    Get max number of points of a signature dataset
    :param dataset: Signature dataset
    :return: max number of points of the dataset
    """
    return max([sig.npoint for sig in dataset])


class DataContainer(object):
    # Simple container with load and save methods.  Declare the container
    # then add data to it.  Save will save any data added to the container.
    # The class automatically gzips the file if it ends in .gz
    #
    # Notes on size and speed (using UbuntuDialog data)
    #       pkl     pkl.gz
    # Save  11.4s   83.7s
    # Load   4.8s   45.0s
    # Size  596M    205M
    #
    @staticmethod
    def is_gzip(filename):
        if filename.split('.')[-1] == 'gz':
            return True
        return False

    # Using HIGHEST_PROTOCOL is almost 2X faster and creates a file that
    # is ~10% smaller.  Load times go down by a factor of about 3X.
    def save(self, filename='DataContainer.pkl'):
        if self.is_gzip(filename):
            f = gzip.open(filename, 'wb')
        else:
            f = open(filename, 'wb')
        pickle.dump(self, f, protocol=pickle.HIGHEST_PROTOCOL)
        f.close()

    # Note that loading to a string with pickle.loads is about 10% faster
    # but probaly comsumes a lot more memory so we'll skip that for now.
    @classmethod
    def load(cls, filename='DataContainer.pkl'):
        if cls.is_gzip(filename):
            f = gzip.open(filename, 'rb')
        else:
            f = open(filename, 'rb')
        n = pickle.load(f)
        f.close()
        return n
