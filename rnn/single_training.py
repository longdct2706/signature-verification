import torch
import torch.nn as nn
from .utils_rnn import get_lstm_output


class SingleRNN(nn.Module):
    """
    Bi-directional RNN architecture
    """

    def __init__(self,
                 input_dim,
                 hidden_dim,
                 feature_dim,
                 output_dim,
                 num_layer,
                 dropout,
                 use_gpu: bool = True,
                 mode: str = 'last'):
        super(SingleRNN, self).__init__()
        # Model
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.feature_dim = feature_dim  # output feature of a signature, used to compare with each other
        self.output_dim = output_dim
        self.num_layer = num_layer
        self.dropout = dropout

        # RNN
        self.lstm = nn.LSTM(input_dim, hidden_dim, num_layer, batch_first=True, bidirectional=True)

        # Dense layer
        # self.fc1 = nn.Linear(hidden_dim * 2, feature_dim)
        # self.fc2 = nn.Linear(feature_dim, output_dim)
        self.fc = nn.Linear(hidden_dim * 2, output_dim)

        # Misc
        self.mode = mode
        self.relu = nn.ReLU()
        self.dropout = nn.Dropout(p=dropout)
        # self.softmax = nn.LogSoftmax(dim=0)
        self.use_gpu = use_gpu

    def sub_forward(self, x):
        batch_size = x.batch_sizes[0].item()

        h = torch.zeros(self.num_layer * 2, batch_size, self.hidden_dim)
        c = torch.zeros(self.num_layer * 2, batch_size, self.hidden_dim)
        if self.use_gpu:
            h, c = h.cuda(), c.cuda()

        out, _ = self.lstm(x, (h, c))
        out, _ = get_lstm_output(out, self.mode)

        # out = self.dropout(out)
        # out = self.fc1(out)

        # out = self.relu(self.fc1(out))
        return out

    def forward(self, x):
        feat_out = self.sub_forward(x)
        out = self.fc(feat_out)
        # out = self.fc2(feat_out)
        # out = self.softmax(out)
        return out, feat_out
