import torch
from torch.nn.utils.rnn import pad_packed_sequence, pack_padded_sequence, pad_sequence
import torch.nn.functional as F


def unpack_seq(seq):
    padded_seq, seq_length = pad_packed_sequence(seq, batch_first=True)
    batch_size = padded_seq.shape[0]

    return [padded_seq[i, :seq_length[i].data, :] for i in range(batch_size)], seq_length


def pack_seq(seq, seq_length):
    padded_seq = pad_sequence(seq, batch_first=True)
    packed_seq = pack_padded_sequence(padded_seq, seq_length, batch_first=True, enforce_sorted=False)

    return packed_seq


def get_lstm_output(output, mode: str = 'last'):
    """
    extract output from lstm layers
    :param output: lstm output
    :param mode: ['last', 'sum', 'avg', 'max']
        'last': get output from last cell
        'sum': get sum of output from each cell
        'avg': get avg of 'sum'
        'max': get maximum of each feature of each cell output
    :return:
    """
    if mode not in ['last', 'sum', 'avg', 'max']:
        raise Exception('Invalid lstm extract mode')
    out, seq_length = unpack_seq(output)
    batch_size = len(out)

    # for i in range(batch_size):
    #     sequence = out[]

    if mode == 'last':
        seq_list = [out[i][-1, :] for i in range(batch_size)]
    elif mode == 'sum':
        seq_list = [torch.sum(out[i][:, :], 0) for i in range(batch_size)]
    elif mode == 'avg':
        seq_list = [torch.mean(out[i][:, :], 0) for i in range(batch_size)]
    else:
        seq_list = [torch.max(out[i][:, :], 0)[0] for i in range(batch_size)]

    return torch.stack(seq_list), seq_length


def concat_2_output(output1, output2):
    out1, seq_length1 = pad_packed_sequence(output1, batch_first=True)
    out2, seq_length2 = pad_packed_sequence(output2, batch_first=True)
    batch_size = out1.shape[0]

    seq_list = []
    for i in range(batch_size):
        seq1 = out1[i, :seq_length1[i], :]
        seq2 = out2[i, :seq_length2[i], :]
        seq_list.append(torch.cat((seq1, seq2), dim=1))

    new_seq_length = [seq_length1[i] + seq_length2[i] for i in range(batch_size)]

    pad_concat_seq = pad_sequence(seq_list, batch_first=True)
    packed_concat_seq = pack_padded_sequence(pad_concat_seq, new_seq_length, batch_first=True, enforce_sorted=False)

    return packed_concat_seq


def distance(output1, output2):
    # distances = (output2 - output1).pow(2).sum(1)  # squared distances
    # return (distances + 1e-9).sqrt()
    return F.pairwise_distance(output1, output2)
