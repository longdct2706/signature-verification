import torch
import torch.nn as nn
from .utils_rnn import get_lstm_output


class PairRNN(nn.Module):
    def __init__(self,
                 input_dim,
                 hidden_dim,
                 num_layer,
                 output_dim,
                 dropout,
                 use_gpu: bool = True,
                 mode: str = 'last'):
        super(PairRNN, self).__init__()
        # Number of hidden dimensions
        self.hidden_dim = hidden_dim

        # Number of hidden layers
        self.num_layer = num_layer

        # RNN
        self.lstm = nn.LSTM(input_dim, hidden_dim, num_layer, batch_first=True, bidirectional=True, dropout=dropout)

        # Readout layer
        self.fc1 = nn.Linear(hidden_dim * 2, output_dim)
        self.fc2 = nn.Linear(output_dim, 1)

        # Misc
        self.mode = mode
        self.relu = nn.ReLU()
        self.use_gpu = use_gpu

    def forward(self, x1, x2):
        # x1: reference signature
        # x2: questioned signature
        batch_size = x1.batch_sizes[0].item()

        h01 = torch.zeros(self.num_layer * 2, batch_size, self.hidden_dim)
        c01 = torch.zeros(self.num_layer * 2, batch_size, self.hidden_dim)

        h02 = torch.zeros(self.num_layer * 2, batch_size, self.hidden_dim)
        c02 = torch.zeros(self.num_layer * 2, batch_size, self.hidden_dim)
        if self.use_gpu:
            h01, c01 = h01.cuda(), c01.cuda()
            h02, c02 = h02.cuda(), c02.cuda()

        out1, hn1 = self.lstm(x1, (h01, c01))
        out2, hn2 = self.lstm(x2, (h02, c02))

        out1, _ = get_lstm_output(out1, self.mode)
        out2, _ = get_lstm_output(out2, self.mode)

        out1 = self.fc1(out1)
        out2 = self.fc1(out2)

        # out = distance(out1, out2)
        # inp = self.relu(out1 - out2)
        inp = torch.abs(out1 - out2)
        # inp = out1 - out2
        out = self.fc2(inp)
        # out = torch.sigmoid(out)
        # out = self.relu(out)

        return out


class Siamese(nn.Module):
    def __init__(self,
                 feature_dim,
                 dropout):
        super(Siamese, self).__init__()

        # Readout layer
        self.fc1 = nn.Linear(feature_dim, feature_dim)
        self.fc2 = nn.Linear(feature_dim, 1)

        # Misc
        self.relu = nn.ReLU()
        self.dropout = nn.Dropout(dropout)
        self.cos = nn.CosineSimilarity(dim=1)

    def forward(self, x1, x2):
        # x1: reference signature
        # x2: questioned signature
        # batch_size = x1.shape[0]
        out1 = self.fc1(x1)
        out2 = self.fc1(x2)

        # out1 = self.dropout(out1)
        # out2 = self.dropout(out2)

        # out = self.cos(out1, out2)
        # out = torch.sqrt(1 - out ** 2)
        # out = out.reshape(batch_size, 1)

        # out = distance(out1, out2)
        # inp = self.relu(out1 - out2)
        merge = torch.abs(out1 - out2)
        # inp = out1 - out2
        out = self.fc2(merge)
        # out = self.dropout(out)
        # out = torch.sigmoid(out)
        # out = self.relu(out)

        return out
